'use client'
import {useRouter} from "next/navigation";
import {useState} from "react";

export default function Page1() {
    const [answer1, setAnswer1] = useState(0);
    const router = useRouter();

    const handleSubmit = (e) => {
        e.preventDefault()
        router.push(`/list/page2?answer1=${answer1}`);
    }

    const handleSelect = (e) => {
        setAnswer1(e.target.value);
        handleSubmit()
    }


    return(
        <form onSubmit={handleSubmit}>
            <div className='text-center'>
                <div className='text-4xl mt-10'>1번 문항 입니다</div>
                <button type="submit" className='border-2 border-black py-4 px-20 mt-10' onClick={handleSelect} value={1}>1번 선택지</button>
                <br/>
                <button type="submit" className='border-2 border-black py-4 px-20 mt-10' onClick={handleSelect} value={2}>2번 선택지</button>
                <br/>
                <button type="submit" className='border-2 border-black py-4 px-20 mt-10' onClick={handleSelect} value={3}>3번 선택지</button>
            </div>
        </form>
    )
}