'use client'

import {useSearchParams} from "next/navigation";

export default function ResultPage() {
    const searchParams = useSearchParams()
    const answer1 = searchParams.get('answer1')
    const answer2 = searchParams.get('answer2')
    const answer3 = searchParams.get('answer3')
    const answer4 = searchParams.get('answer4')
    const answer5 = searchParams.get('answer5')
    const answer6 = searchParams.get('answer6')
    const answer7 = searchParams.get('answer7')
    const answer8 = searchParams.get('answer8')
    const answer9 = searchParams.get('answer9')
    const answer10 = searchParams.get('answer10')

    const sum = () => {
        return Number(answer1) + Number(answer2) + Number(answer3) + Number(answer4) + Number(answer5) + Number(answer6) + Number(answer7) + Number(answer8) + Number(answer9) + Number(answer10)
    }

    return (
        <div className='text-center'>
            <div className='text-4xl mt-10'>결과</div>
            <div className='text-2xl mt-10'>당신의 점수는 {sum()}점</div>
        </div>
    )
}