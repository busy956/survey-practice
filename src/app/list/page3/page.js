'use client'
import {useRouter, useSearchParams} from "next/navigation";
import {useState} from "react";

export default function Page3() {
    const [answer3, setAnswer3] = useState(0);
    const router = useRouter();
    const searchParams = useSearchParams()
    const answer1 = searchParams.get('answer1')
    const answer2 = searchParams.get('answer2')

    const handleSubmit = (e) => {
        e.preventDefault()
        router.push(`/list/page4?answer1=${answer1}&answer2=${answer2}&answer3=${answer3}`);
    }

    const handleSelect = (e) => {
        setAnswer3(e.target.value);
        handleSubmit()
    }


    return(
        <form onSubmit={handleSubmit}>
            <div className='text-center'>
                <div className='text-4xl mt-10'>3번 문항 입니다</div>
                <button type="submit" className='border-2 border-black py-4 px-20 mt-10' onClick={handleSelect} value={1}>1번 선택지</button>
                <br/>
                <button type="submit" className='border-2 border-black py-4 px-20 mt-10' onClick={handleSelect} value={2}>2번 선택지</button>
                <br/>
                <button type="submit" className='border-2 border-black py-4 px-20 mt-10' onClick={handleSelect} value={3}>3번 선택지</button>
            </div>
        </form>
    )
}