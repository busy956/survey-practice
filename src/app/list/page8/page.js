'use client'
import {useRouter, useSearchParams} from "next/navigation";
import {useState} from "react";

export default function Page8() {
    const [answer8, setAnswer8] = useState(0);
    const router = useRouter();
    const searchParams = useSearchParams()
    const answer1 = searchParams.get('answer1')
    const answer2 = searchParams.get('answer2')
    const answer3 = searchParams.get('answer3')
    const answer4 = searchParams.get('answer4')
    const answer5 = searchParams.get('answer5')
    const answer6 = searchParams.get('answer6')
    const answer7 = searchParams.get('answer7')

    const handleSubmit = (e) => {
        e.preventDefault()
        router.push(`/list/page9?answer1=${answer1}&answer2=${answer2}&answer3=${answer3}&answer4=${answer4}&answer5=${answer5}&answer6=${answer6}&answer7=${answer7}&answer8=${answer8}`);
    }

    const handleSelect = (e) => {
        setAnswer8(e.target.value);
        handleSubmit()
    }


    return(
        <form onSubmit={handleSubmit}>
            <div className='text-center'>
                <div className='text-4xl mt-10'>8번 문항 입니다</div>
                <button type="submit" className='border-2 border-black py-4 px-20 mt-10' onClick={handleSelect} value={1}>1번 선택지</button>
                <br/>
                <button type="submit" className='border-2 border-black py-4 px-20 mt-10' onClick={handleSelect} value={2}>2번 선택지</button>
                <br/>
                <button type="submit" className='border-2 border-black py-4 px-20 mt-10' onClick={handleSelect} value={3}>3번 선택지</button>
            </div>
        </form>
    )
}