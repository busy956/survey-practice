'use client'
import {useRouter} from "next/navigation";

export default function PageList() {
    const router = useRouter()

    return (
        <div className='text-center'>
            <div className='text-2xl mt-10'>1. 총 10개의 설문이 있습니다</div>
            <div className='text-2xl mt-10'>2. 맞다고 생각되는 선택지를 골라주세요</div>
            <div className='text-2xl mt-10'>3. 그러면 이제 시작해보도록 하겠습니다!</div>
            <div>
                <button onClick={() => router.push('/list/page1')} className='border-2 py-2 px-4 mt-10'>시작</button>
            </div>
        </div>
    )
}