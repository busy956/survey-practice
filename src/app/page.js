'use client'
import {useRouter} from "next/navigation";

export default function Home() {
  const router = useRouter();

  return (
    <div className='text-center'>
      <div className='text-4xl mt-10'>설문지</div>
      <button onClick={() => router.push('/list')} className='border-2 py-2 px-4 mt-10'>시작</button>
    </div>
  );
}
